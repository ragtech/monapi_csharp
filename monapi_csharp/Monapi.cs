﻿using System;
using System.Runtime.InteropServices;

namespace monapi_csharp
{
    public class Monapi_Flags
    {
        const int MONAPI_ERRORCODE_SUCCESS = 0;
        const int MONAPI_ERRORCODE_INVALID_PARAM = 1;
        const int MONAPI_ERRORCODE_MALLOC_ERROR = 2;
        const int MONAPI_ERRORCODE_LOADLIB_ERROR = 3;
        const int MONAPI_ERRORCODE_FILEIO_ERROR = 4;
        const int MONAPI_ERRORCODE_MONSVC_ERROR = 5;

        const int MONAPI_OPMODE_DISCONNECTED = 0;
        const int MONAPI_OPMODE_STARTUP = 1;
        const int MONAPI_OPMODE_STANDBY = 2;
        const int MONAPI_OPMODE_BATTERY = 3;
        const int MONAPI_OPMODE_LINE = 4;
        const int MONAPI_OPMODE_WARNING = 5;

        const int MONAPI_STATUS_DISCONNECTED = 0;
        const int MONAPI_STATUS_STARTUP = 1;
        const int MONAPI_STATUS_LOWBAT = 2;
        const int MONAPI_STATUS_LINEOK = 3;
        const int MONAPI_STATUS_NOLINE = 4;
        const int MONAPI_STATUS_LOWLINE = 5;
        const int MONAPI_STATUS_HIGHLINE = 6;
        const int MONAPI_STATUS_LOWFREQ = 7;
        const int MONAPI_STATUS_HIGHFREQ = 8;
        const int MONAPI_STATUS_UNCALIB = 9;
        const int MONAPI_STATUS_FANERROR = 10;
        const int MONAPI_STATUS_NOLINESYNC = 11;
        const int MONAPI_STATUS_FAIL_ENDBAT = 12;
        const int MONAPI_STATUS_FAIL_VERYLOWLINE = 13;
        const int MONAPI_STATUS_FAIL_OVERTEMP = 14;
        const int MONAPI_STATUS_FAIL_INTERNAL = 15;
        const int MONAPI_STATUS_FAIL_OVERLOAD = 16;
        const int MONAPI_STATUS_FAIL_SHORTCIRCUIT = 17;
        const int MONAPI_STATUS_FAIL_VOUT = 18;
        const int MONAPI_STATUS_FAIL_VBAT = 19;
        const int MONAPI_STATUS_FAIL_INVERTER = 20;
        const int MONAPI_STATUS_FAIL_POUTHIGH = 21;
        const int MONAPI_STATUS_FAIL_TIRISTOR = 22;

        /* Converte codigo de erro para string descrevendo o erro */
        public static string ErrorCodeToStr(int errorcode)
        {
            switch (errorcode)
            {
                case MONAPI_ERRORCODE_SUCCESS: return "MONAPI_ERRORCODE_SUCCESS";
                case MONAPI_ERRORCODE_INVALID_PARAM: return "MONAPI_ERRORCODE_INVALID_PARAM";
                case MONAPI_ERRORCODE_MALLOC_ERROR: return "MONAPI_ERRORCODE_MALLOC_ERROR";
                case MONAPI_ERRORCODE_LOADLIB_ERROR: return "MONAPI_ERRORCODE_LOADLIB_ERROR";
                case MONAPI_ERRORCODE_FILEIO_ERROR: return "MONAPI_ERRORCODE_FILEIO_ERROR";
                case MONAPI_ERRORCODE_MONSVC_ERROR: return "MONAPI_ERRORCODE_MONSVC_ERROR";
                default: return "ERRO_DESCONHECIDO";
            }
        }

        /* Converte codigo opmode para string identificando o modo de operacao */
        public static string OpModeToStr(int opmode)
        {
            switch (opmode)
            {
                case MONAPI_OPMODE_DISCONNECTED: return "MONAPI_OPMODE_DISCONNECTED";
                case MONAPI_OPMODE_STARTUP: return "MONAPI_OPMODE_STARTUP";
                case MONAPI_OPMODE_STANDBY: return "MONAPI_OPMODE_STANDBY";
                case MONAPI_OPMODE_BATTERY: return "MONAPI_OPMODE_BATTERY";
                case MONAPI_OPMODE_LINE: return "MONAPI_OPMODE_LINE";
                case MONAPI_OPMODE_WARNING: return "MONAPI_OPMODE_WARNING";
                default: return "MODO_DE_OP_DESCONHECIDO";
            }
        }

        /* Converte codigo de status para string descrevendo o status */
        public static string StatusToStr(int status)
        {
            switch (status)
            {
                case MONAPI_STATUS_DISCONNECTED: return "MONAPI_STATUS_DISCONNECTED";
                case MONAPI_STATUS_STARTUP: return "MONAPI_STATUS_STARTUP";
                case MONAPI_STATUS_LOWBAT: return "MONAPI_STATUS_LOWBAT";
                case MONAPI_STATUS_LINEOK: return "MONAPI_STATUS_LINEOK";
                case MONAPI_STATUS_NOLINE: return "MONAPI_STATUS_NOLINE";
                case MONAPI_STATUS_LOWLINE: return "MONAPI_STATUS_LOWLINE";
                case MONAPI_STATUS_HIGHLINE: return "MONAPI_STATUS_HIGHLINE";
                case MONAPI_STATUS_LOWFREQ: return "MONAPI_STATUS_LOWFREQ";
                case MONAPI_STATUS_HIGHFREQ: return "MONAPI_STATUS_HIGHFREQ";
                case MONAPI_STATUS_UNCALIB: return "MONAPI_STATUS_UNCALIB";
                case MONAPI_STATUS_FANERROR: return "MONAPI_STATUS_FANERROR";
                case MONAPI_STATUS_NOLINESYNC: return "MONAPI_STATUS_NOLINESYNC";
                case MONAPI_STATUS_FAIL_ENDBAT: return "MONAPI_STATUS_FAIL_ENDBAT";
                case MONAPI_STATUS_FAIL_VERYLOWLINE: return "MONAPI_STATUS_FAIL_VERYLOWLINE";
                case MONAPI_STATUS_FAIL_OVERTEMP: return "MONAPI_STATUS_FAIL_OVERTEMP";
                case MONAPI_STATUS_FAIL_INTERNAL: return "MONAPI_STATUS_FAIL_INTERNAL";
                case MONAPI_STATUS_FAIL_OVERLOAD: return "MONAPI_STATUS_FAIL_OVERLOAD";
                case MONAPI_STATUS_FAIL_SHORTCIRCUIT: return "MONAPI_STATUS_FAIL_SHORTCIRCUIT";
                case MONAPI_STATUS_FAIL_VOUT: return "MONAPI_STATUS_FAIL_VOUT";
                case MONAPI_STATUS_FAIL_VBAT: return "MONAPI_STATUS_FAIL_VBAT";
                case MONAPI_STATUS_FAIL_INVERTER: return "MONAPI_STATUS_FAIL_INVERTER";
                case MONAPI_STATUS_FAIL_POUTHIGH: return "MONAPI_STATUS_FAIL_POUTHIGH";
                case MONAPI_STATUS_FAIL_TIRISTOR: return "MONAPI_STATUS_FAIL_TIRISTOR";
                default: return "STATUS_DESCONHECIDO";
            }
        }
    }

    public class Monapi
    {
        [StructLayout(LayoutKind.Sequential), Serializable]
        public struct MONAPI_STATUS_STRUCT
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 30)] public string model;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)] public string version;
            public Int32 op_mode;
            public Int32 status;
            public Int32 regressive_counter;
            public Int32 battery_full_discharge;
            public float vin;
            public float vout;
            public Int32 pout; 
            public float fout;
            public float vbat;
            public float temp; 
            public Int32 batstatus;
        }

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetMonitorVersion();

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr Start(int handle);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int Stop(IntPtr handle);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetDevice(IntPtr handle, int index);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetStatus(IntPtr handle, ref MONAPI_STATUS_STRUCT status , int index);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int ManualShutdown(IntPtr handle, int index);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int CancelCounterShutdown(IntPtr handle, int index);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int ChangeColor(IntPtr handle, int red, int green, int blue, int index);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int BatteryFullDischarge(IntPtr handle, int index);

        [DllImport("monapi.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)] 
        public static extern int GetMonitorLastError();
    }
}

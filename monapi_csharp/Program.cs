﻿using System;
using System.Runtime.InteropServices;

namespace monapi_csharp
{
    class Program
    {
        public static Monapi.MONAPI_STATUS_STRUCT ssStatus;
        static void Main(string[] args)
        {
            ssStatus = new Monapi.MONAPI_STATUS_STRUCT();
            ConsoleKey cKey;
            int index = 0;

            //-----------------------------------------------------------------------------------//
            //------------------------- Inicie o monitoramento => Start() -----------------------//
            //-----------------------------------------------------------------------------------//

            IntPtr hMon = Monapi.Start(0);
            if (hMon == null)
            {
                Console.WriteLine("Erro em Start()." + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()));
                return;
            }

            //-----------------------------------------------------------------------------------//
            //------------------- Permite listar os dispositivos => GetDevice() -----------------//
            //-----------------------------------------------------------------------------------//

            cKey = 0;
            while (cKey != ConsoleKey.Escape && (cKey < ConsoleKey.F1 || cKey > ConsoleKey.F12)) {
                index = 0;
                String device;
                Console.Clear();
                Console.WriteLine(" |---------- Programa de Teste da API de monitoramento ---------|");
                Console.WriteLine(" |----------    Selecione um equipamento na lista:    ----------|");
                Console.WriteLine(" ");
                do
                {
                    device = Marshal.PtrToStringAnsi(Monapi.GetDevice(hMon, index++));
                    if (device != "")
                        Console.WriteLine("Pressione F" + index + " para selecionar o equipamento de ID: " + device);
                } while (device != "");
                Console.WriteLine("Pressione ESC para continuar");
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo info = Console.ReadKey();
                    cKey = info.Key;
                    switch (cKey)
                    {
                        case ConsoleKey.Escape:
                        case ConsoleKey.F1: index = 0; break;
                        case ConsoleKey.F2: index = 1; break;
                        case ConsoleKey.F3: index = 2; break;
                        case ConsoleKey.F4: index = 3; break;
                        case ConsoleKey.F5: index = 4; break;
                        case ConsoleKey.F6: index = 5; break;
                        case ConsoleKey.F7: index = 6; break;
                        case ConsoleKey.F8: index = 7; break;
                        case ConsoleKey.F9: index = 8; break;
                        case ConsoleKey.F10: index = 9; break;
                        case ConsoleKey.F11: index = 10; break;
                        case ConsoleKey.F12: index = 11; break;
                    }
                }
                System.Threading.Thread.Sleep(1000);
            }

            //-----------------------------------------------------------------------------------//
            //------------------- Permite monitorar o dispositivo => GetStatus() ----------------//
            //-----------------------------------------------------------------------------------//

            while (cKey != ConsoleKey.Escape)
            {
                Console.Clear();
                Console.WriteLine(" |----------- Programa de Teste da API de monitoramento ----------|");
                Console.WriteLine(" | ESC: Sair        | F1: Shutdown Manual | F2: Cancelar Shutdown |");
                Console.WriteLine(" | F3: Led Vermelho | F4: Led Verde       | F5: Led Azul          |");
                Console.WriteLine(" ");
                Console.WriteLine("Versao do Monitoramento  : " + Marshal.PtrToStringAnsi(Monapi.GetMonitorVersion()));

                bool erroStatus = false;
                if (Monapi.GetStatus(hMon, ref ssStatus, index) == 0)
                    erroStatus = true;

                Console.WriteLine("Modelo do Equipamento   : " + ssStatus.model);
                Console.WriteLine("Versão do Firmware      : " + ssStatus.version);
                Console.WriteLine(" ");

                if (ssStatus.regressive_counter==-1)
                    Console.WriteLine("Contagem Regressiva     : " + "N/A");
                else
                    Console.WriteLine("Contagem Regressiva     : " + ssStatus.regressive_counter + " Segundos");
                Console.WriteLine(" ");

                Console.WriteLine("Modo de Operação        : " + Monapi_Flags.OpModeToStr(ssStatus.op_mode));
                Console.WriteLine("Status do Equipamento   : " + Monapi_Flags.StatusToStr(ssStatus.status));
                Console.WriteLine(" ");

                Console.WriteLine("Tensão de Entrada       : " + ssStatus.vin + " Volts");
                Console.WriteLine("Tensão de Saída         : " + ssStatus.vout + " Volts");
                Console.WriteLine("Tensão de Bateria       : " + ssStatus.vbat + " Volts");
                Console.WriteLine("Temperatura             : " + ssStatus.temp + " Graus C");
                Console.WriteLine("Carga de Bateria        : " + ssStatus.batstatus + " Por Cento");
                Console.WriteLine("Potência de Saída       : " + ssStatus.pout + " Por Cento");
                Console.WriteLine("Frequência de Saída     : " + ssStatus.fout + " Hertz");
                Console.WriteLine(" ");

                if (erroStatus)
                    Console.WriteLine("Erro " + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()) + " em GetStatus()");

                //-----------------------------------------------------------------------------------//
                //---- Permite enviar comandos para o dispositivo => ManualShutdown() e outros.. ----//
                //-----------------------------------------------------------------------------------//

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo info = Console.ReadKey();
                    cKey = info.Key;
                    switch (cKey)
                    {
                        case ConsoleKey.F1:
                            if (Monapi.ManualShutdown(hMon, index) != 0)
                                Console.WriteLine("Shutdown Manual Iniciado.");
                            else
                                Console.WriteLine("Erro " + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()) + " em ManualShutdown()");
                            break;
                        case ConsoleKey.F2:
                            if (Monapi.CancelCounterShutdown(hMon, index) != 0)
                                Console.WriteLine("Shutdown Cancelado.");
                            else
                                Console.WriteLine("Erro " + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()) + " em ManualShutdown()");
                            break;
                        case ConsoleKey.F3:
                            if (Monapi.ChangeColor(hMon, 255, 0, 0, index) != 0)
                                Console.WriteLine("Cor alterada para vermelho.");
                            else
                                Console.WriteLine("Erro " + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()) + " em ChangeColor()");
                            break;
                        case ConsoleKey.F4:
                            if (Monapi.ChangeColor(hMon, 0, 255, 0, index) != 0)
                                Console.WriteLine("Cor alterada para verde.");
                            else
                                Console.WriteLine("Erro " + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()) + " em ChangeColor()");
                            break;
                        case ConsoleKey.F5:
                            if (Monapi.ChangeColor(hMon, 0, 0, 255, index) != 0)
                                Console.WriteLine("Cor alterada para azul.");
                            else
                                Console.WriteLine("Erro " + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()) + " em ChangeColor()");
                            break;
                    }
                }
                System.Threading.Thread.Sleep(1000);
            }

            //-----------------------------------------------------------------------------------//
            //--------------------------- Encerra o monitoramento => Stop() ---------------------//
            //-----------------------------------------------------------------------------------//

            if (Monapi.Stop(hMon) == 0)
                Console.WriteLine("Erro " + Monapi_Flags.ErrorCodeToStr(Monapi.GetMonitorLastError()) + " em Stop().");
        }
    }
}
